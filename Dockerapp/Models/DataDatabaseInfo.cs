using System.ComponentModel.DataAnnotations.Schema;

namespace Dockerapp.Models;


public class JsonDataInfo
{
    [Column(TypeName = "jsonb")] 
    public string Data { get; set; } = null!;
}

public record DataDatabaseInfo(string Response, string? Request, int? StatusCode);
