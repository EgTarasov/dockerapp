using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Text.Json;
using Dapper;
using Dockerapp.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Npgsql;

namespace Dockerapp.Controllers;


[ApiController]
[Route("[controller]")]
public class MainController: ControllerBase
{
    private ILogger<MainController> _logger;
    
    private string _connectionString;

    public MainController(
        IOptions<PostgresOptions> postgresOptions,
        ILogger<MainController> logger)
    {
        _connectionString = postgresOptions.Value.ConnectionString;
        _logger = logger;
    }
    

    [HttpPost("Solve")]
    public async Task<ActionResult> Solve(double number1 = 1, double number2 = 1, Operations op = Operations.Plus)
    {
        try
        {
            if (number2 == 0 && op is Operations.Divide)
                throw new DivideByZeroException();

            double value;
            switch (op)
            {
                case Operations.Plus:
                    value = number1 + number2;
                    break;
                case Operations.Minus:
                    value = number1 - number2;
                    break;
                case Operations.Multiply:
                    value = number1 * number2;
                    break;
                case Operations.Divide:
                    value = number1 / number2;
                    break;
                default:
                    throw new ArgumentException("Invalid operation");
            }

            if (double.IsInfinity(value))
            {
                throw new ArgumentException("Infinity");
            }
            return await AddRequestInfo(Request, base.Ok(value));
        }
        catch (DivideByZeroException ex)
        {
            _logger.LogWarning("Division by zero", ex);
            return await AddRequestInfo(Request, base.Conflict("Division by zero"));
        }
        catch (Exception ex)
        {
            _logger.LogWarning(ex.Message, ex);
            return await AddRequestInfo(Request, base.StatusCode(500, "Unexpected outcome, try again!"));
        }
    }
    
    public enum Operations
    {
        Undefined = 0,
        Plus = 1,
        Minus = 2,
        Multiply = 3,
        Divide = 4,
    }

    /// <summary>
    /// Add info about request and response into database
    /// </summary>
    /// <returns>return response</returns>
    [NonAction]
    private async Task<ObjectResult> AddRequestInfo(HttpRequest request, ObjectResult response)
    {
        var conn = new NpgsqlConnection(_connectionString);

        var url = request.Url();

        var data = new DataDatabaseInfo(
            url,
            response.Value?.ToString()!, response.StatusCode);

        var command = "INSERT INTO public.requests_info(handler, data) VALUES(@Handler::jsonb, @Data::jsonb)";
       
        await conn.ExecuteAsync(
            command,
            new
            {
                Handler = $"{{\"{Request.Method}\" : \"{request.Path.Value}\"}}",
                Data = JsonSerializer.Serialize(data)
            });
        
        return response;
    }
}


public static class RequestExtension
{
    /// <summary>
    /// Create url from request
    /// </summary>
    /// <param name="request">Info about request</param>
    /// <returns>Url</returns> P.S Не нашел в core готовй функции
    public static string Url(this HttpRequest request)
    {
        var host = request.Host.Value;
        return $"{request.Scheme}://{host}{request.Path.Value}{request.QueryString.Value}";
    }
}
