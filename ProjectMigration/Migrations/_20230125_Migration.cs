using FluentMigrator;

namespace Sem1701Migrations.Migrations;


[Migration(20230125, "requests_info table")]
public class _20230125_Migration: Migration
{
    private const string scriptUp =
        @"
        create table public.requests_info
            (
            id      bigserial constraint pk_id primary key,
            handler jsonb,
            data    jsonb
            );
         ";

    private const string scriptDown =
        @"
        Drop table public.requests_info;
        ";
    
    
    public override void Up()
    {
        Execute.Sql(scriptUp);
    }

    public override void Down()
    {
        Execute.Sql(scriptDown);
    }
}