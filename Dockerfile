﻿FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /src
COPY ["Dockerapp/Dockerapp.csproj", "Dockerapp/"]
RUN dotnet restore "Dockerapp/Dockerapp.csproj"
COPY . .
WORKDIR "/src/Dockerapp"
RUN dotnet build "Dockerapp.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "Dockerapp.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "Dockerapp.dll"]
