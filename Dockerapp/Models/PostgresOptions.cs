namespace Dockerapp.Models;

public class PostgresOptions
{
    public string ConnectionString { get; set; }
}